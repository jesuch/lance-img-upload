import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { AngularFireModule } from '@angular/fire';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { environment } from '../environments/environment';
import { ReactiveFormsModule } from '@angular/forms';

import { ImagesComponent } from './features/component/images/images.component';
import { UploadComponent } from './features/container/upload/upload.component';
import { ImageListComponent } from './features/component/image-list/image-list.component';
import { ImageComponent } from './features/component/image/image.component';

import { ImageService } from './shared/image.service';
import { ImageDetailsComponent } from './features/component/image-details/image-details.component';

@NgModule({
  declarations: [
    AppComponent,
    ImagesComponent,
    ImageComponent,
    ImageListComponent,
    UploadComponent,
    ImageDetailsComponent
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireStorageModule,
    AngularFireDatabaseModule,

    ReactiveFormsModule,
  
  ],
  providers: [ImageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
