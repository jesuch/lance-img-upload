import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';
import { ImageService } from 'src/app/shared/image.service';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.scss']
})
export class UploadComponent implements OnInit {
  imgSrc: string;
  selectedImage: any = null;
  isSubmitted: boolean;

  @Output() image: EventEmitter<any> = new EventEmitter<any>();

  constructor(private storage: AngularFireStorage, private service: ImageService) { }

  ngOnInit() {

  }

  onWineFormSubmit(a) {
    console.log(a);
  }

  onUploadSubmit(formValue) {
    console.log(formValue.hiddenImage.name);
    const filePath = `${formValue.category}/${formValue.hiddenImage.name.split('.').slice(0, -1).join('.')}_${new Date().getTime()}`;
    const fileRef = this.storage.ref(filePath);
    this.storage.upload(filePath, formValue.hiddenImage).snapshotChanges().pipe(
      finalize(() => {
        fileRef.getDownloadURL().subscribe((url) => {

          // tslint:disable-next-line: no-string-literal
          formValue['imageUrl'] = url;

          // CONDITION TO FILE FOR EACH FOLDER
          if (formValue.category === 'A Mano') { this.service.insertAMano(formValue); }
          if (formValue.category === 'Hardys and Sons') { this.service.insertHardysAndSons(formValue); }
          if (formValue.category === 'YellowTail') { this.service.insertYellowTail(formValue); }
          if (formValue.category === 'Barefoot') { this.service.insertBarefoot(formValue); }
          if (formValue.category === 'Concha Toro') { this.service.insertConchaToro(formValue); }
          if (formValue.category === 'Others') { this.service.insertOthers(formValue); }
        });
      })
    ).subscribe();
  }
}
