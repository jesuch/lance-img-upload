import { ImageService } from './../../../shared/image.service';
import { Component, OnInit, Input } from '@angular/core';
import { ImageComponent } from './../image/image.component';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { AngularFireStorage } from '@angular/fire/storage';

@Component({
  selector: 'app-image-details',
  templateUrl: './image-details.component.html',
  styleUrls: ['./image-details.component.scss']
})

export class ImageDetailsComponent implements OnInit {
  formTemplate: FormGroup;

  constructor(
    private storage: AngularFireStorage,
    private service: ImageService,
    private fb: FormBuilder) {

      this.formTemplate = this.fb.group({
        $key: [null],
        caption: ['', Validators.required],
        category: [''],
        // hiddenImage: [''],   // Image Transfer
        imageUrl: ['', Validators.required],
        description: [''],
        allergen: [''],
        vintage: [''],
        country: [''],
        region: [''],
        type: [''],
        alcohol: [''],
      });
    }
// TESTING AREA

  /* formComponent = ImageComponent.formTransfer; */
  /* imageDetailsList.formTemplate */
  /* ImageComponent.formTransfer; */

  @Input() public form: string;


// TESTING AREA


  winesYellowTail: any[];
  winesHardys: any[];
  winesAMano: any[];
  winesBarefoot: any[];
  winesConchaToro: any[];
  winesGallo: any[];
  winesOthers: any[];

  ngOnInit() {
    // YellowTail
    this.service.getImageDetailList().subscribe(
      list => {
        this.winesYellowTail = list.map(item => {
          return {
            $key: item.key,
            ...item.payload.val()
          };
        });
      });

    // Hardys And Sons
    this.service.getImageHardys().subscribe(
      list => {
        this.winesHardys = list.map(item => {
          return {
            $key: item.key,
            ...item.payload.val()
          };
        });
      });

    // AMano
    this.service.getImageAMano().subscribe(
      list => {
        this.winesAMano = list.map(item => {
          return {
            $key: item.key,
            ...item.payload.val()
          };
        });
      });

    // Barefoot
    this.service.getImageBarefoot().subscribe(
      list => {
        this.winesBarefoot = list.map(item => {
          return {
            $key: item.key,
            ...item.payload.val()
          };
        });
      });

    // Concha Toro
    this.service.getImageConchaToro().subscribe(
      list => {
        this.winesConchaToro = list.map(item => {
          return {
            $key: item.key,
            ...item.payload.val()
          };
        });
      });

    // Gallo
    this.service.getImageGallo().subscribe(
      list => {
        this.winesGallo = list.map(item => {
          return {
            $key: item.key,
            ...item.payload.val()
          };
        });
      });

    // Others
    this.service.getImageOthers().subscribe(
      list => {
        this.winesOthers = list.map(item => {
          return {
            $key: item.key,
            ...item.payload.val()
          };
        });
      });
  }

  onDelete($key, downloadUrl) {
    if (confirm('Are you sure to delete this record ?')) {
      this.service.deleteWines($key);
      this.service.onImageDelete(downloadUrl);
      // this.showDeletedMessage = true;
      // setTimeout(() => this.showDeletedMessage = false, 3000);
    }
  }

  populateForm(wines) {
    this.formTemplate.setValue(wines);
  }

  onSubmit() {
    this.service.updateYellow(this.formTemplate.value);
  }

  onSubmitHardy() {
    this.service.updateHardy(this.formTemplate.value);
  }

  onSubmitAMano() {
    this.service.updateAMano(this.formTemplate.value);
  }

  onSubmitBarefoot() {
    this.service.updateBarefoot(this.formTemplate.value);
  }

  onSubmitConcha() {
    this.service.updateConcha(this.formTemplate.value);
  }

  onSubmitGallo() {
    this.service.updateGallo(this.formTemplate.value);
  }

  onSubmitOthers() {
    this.service.updateOthers(this.formTemplate.value);
  }

}
