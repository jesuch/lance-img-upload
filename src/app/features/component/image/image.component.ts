import { ImageService } from '../../../shared/image.service';
import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { AngularFireStorage } from '@angular/fire/storage';
import { finalize } from 'rxjs/operators';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';

@Component({
  selector: 'app-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.scss']
})
export class ImageComponent implements OnInit {
  imgSrc: string;
  selectedImage: any = null;
  isSubmitted: boolean;
  formTemplate: FormGroup;
  winesList: any[];

  public formTransfer = this.formTemplate;
  public givenForm = this.formTemplate;

  @Output() formSubmit = new EventEmitter<any>();
  @Output() wineFormSubmit = new EventEmitter<any>();

  ImageService: any;

  constructor(
    private storage: AngularFireStorage,
    private service: ImageService,
    private fb: FormBuilder) {

    this.formTemplate = this.fb.group({

      caption: ['', Validators.required],
      category: [''],
      hiddenImage: [''],  // Image Transfer
      imageUrl: ['', Validators.required],
      description: [''],
      allergen: [''],
      vintage: [''],
      country: [''],
      region: [''],
      type: [''],
      alcohol: ['']
    });
  }

  ngOnInit() {
    this.resetform();
  }

  showPreview(event: any) {
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      reader.onload = (e: any) => this.imgSrc = e.target.result;
      reader.readAsDataURL(event.target.files[0]);
      this.formTemplate.get('hiddenImage').patchValue(event.target.files[0]);
    }
    // tslint:disable-next-line: one-line
    else {
      this.imgSrc = '/assets/img/cat_headphones.png';
      this.formTemplate.get('hiddenImage').patchValue(this.imgSrc);
    }
  }
  onSubmit() {
    // if (this.formTemplate.valid) {
    // if (this.ImageService.form.get('$key').value == null) {
    this.formSubmit.emit(this.formTemplate.value);
    this.wineFormSubmit.emit('hello world');
    this.resetform();
    // } else {
    //   this.formTemplate.get('caption').markAsTouched();
    // }
  }

  get formControls() {
    // tslint:disable-next-line: no-string-literal
    return this.formTemplate['controls'];
  }

  resetform() {
    this.formTemplate.reset();
    this.formTemplate.setValue({
      caption: '',
      imageUrl: '',
      hiddenImage: '',
      category: '',
      description: [''],
      allergen: [''],
      vintage: [''],
      country: [''],
      region: [''],
      type: [''],
      alcohol: ['']
    });
    this.imgSrc = '/assets/img/cat_headphones.png';
    this.selectedImage = null;
    this.isSubmitted = false;
  }
  // // tslint:disable-next-line: variable-name
  // SearchData(_no: string) {
  //   // console.log(_no);
  //   this.valueQty = this.formTemplate.controls.valueAddedTax;
  //   console.log(this.valueQty);
  // }
}
