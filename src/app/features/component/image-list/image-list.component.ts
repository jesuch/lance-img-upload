import { Component, OnInit } from '@angular/core';
import { ImageService } from '../../../shared/image.service';

@Component({
  selector: 'app-image-list',
  templateUrl: './image-list.component.html',
  styleUrls: ['./image-list.component.scss']
})
export class ImageListComponent implements OnInit {

imageList: any[];

amanoList: any[];
hardyList: any[];
yellowList: any[];
barefootList: any[];
conchaList: any[];
galloList: any[];
othersList: any[];

rowIndexArray: any[];
rowHardyArray: any[];

  constructor(private service: ImageService) { }

  ngOnInit() {
    // AMANO DISPLAY
    this.service.imageAMano.snapshotChanges().subscribe(
      list => {
        this.amanoList = list.map(item => {
          return item.payload.val(); });
        this.rowIndexArray = Array.from(Array(Math.ceil(this.amanoList.length / 3)).keys());
      }
    );

    // HARDY AND SONS DISPLAY
    this.service.imageHardysAndSons.snapshotChanges().subscribe(
      lists => {
        this.hardyList = lists.map(items => {
          return items.payload.val(); });
        this.rowIndexArray = Array.from(Array(Math.ceil(this.hardyList.length / 3)).keys());
      }
    );

    // YELLOWTAIL DISPLAY
    this.service.imageYellowTail.snapshotChanges().subscribe(
      list => {
        this.yellowList = list.map(item => {
          return item.payload.val(); });
        this.rowIndexArray = Array.from(Array(Math.ceil(this.yellowList.length / 3)).keys());
      }
    );

    // BAREFOOT DISPLAY
    this.service.imageBarefoot.snapshotChanges().subscribe(
      list => {
        this.barefootList = list.map(item => {
          return item.payload.val(); });
        this.rowIndexArray = Array.from(Array(Math.ceil(this.barefootList.length / 3)).keys());
      }
    );

    // CONCHA TORO DISPLAY
    this.service.imageConchaToro.snapshotChanges().subscribe(
      list => {
        this.conchaList = list.map(item => {
          return item.payload.val(); });
        this.rowIndexArray = Array.from(Array(Math.ceil(this.conchaList.length / 3)).keys());
      }
    );

    // GALLO DISPLAY
    this.service.imageGallo.snapshotChanges().subscribe(
      list => {
        this.galloList = list.map(item => {
          return item.payload.val(); });
        this.rowHardyArray = Array.from(Array(Math.ceil(this.galloList.length / 3)).keys());
      }
    );

    // OTHERS DISPLAY
    this.service.imageOthers.snapshotChanges().subscribe(
      list => {
        this.othersList = list.map(item => {
          return item.payload.val(); });
        this.rowHardyArray = Array.from(Array(Math.ceil(this.othersList.length / 3)).keys());
      }
    );
  }
}
