import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ImageListComponent } from './features/component/image-list/image-list.component';
import { ImagesComponent } from './features/component/images/images.component';
import { UploadComponent } from './features/container/upload/upload.component';


const routes: Routes = [
  { path: '', redirectTo: 'images/upload', pathMatch: 'full' },
  {
    path: 'images', component: ImagesComponent,
    children: [
      {path: 'upload', component: UploadComponent}, // image/upload
      {path: 'list', component: ImageListComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
